# Unsupervised Particle detection in 3D tomograms

## Abstract 
We propose an unsupervised particle detection algorithm to extract 3D protein particles from 3D tomograms. Our algorithm no longer requires human labeling and reduce the time for training data. Users could tune hyperparameters to improve the performance.

## Installation
Recommended steps to install the platform (MacOS and Linux are supported):
1. Install anaconda if not already installed https://www.anaconda.com/download/
2. Create an Anaconda environment ```conda create -n picker_env python=3.6.8 -y``` and get into the environment ```conda activate picker_env```
3. Install required packages in ```requirements.txt```
4. Install Imod https://bio3d.colorado.edu/imod/

## Particle picking

### File locations
Main python file picker.py with other helper python files are in the main directory. The tomograms should be presented as .rec files (e.g. name.rec). Different kinds of output files such as mtf-filtered tomograms (e.g. name_bp.mrc), contamination masks (e.g. name_area.rec) and particle positions (e.g. name.spk) would be generated in the user decided output directory. Imod package could be put in a suitable place like other applications (e.g. /Applications/IMOD in Mac).

### Running the main codes
A sample command to pick particles from a tomogram with location '/tomo/TS_86.rec' while output would be put in the directory '/output':
```
python3 picker.py  --imod-location /Applications/IMOD --input /tomo/TS_86.rec --output /output --rad 90 --scope-pixel 1.7 --binning 8 --contract-times-3d 1 --gaussian-3d False --sigma-3d 15 --stdtimes-cont-3d 0.75 --min-size-3d 125 --dilation-3d 70 --radiustimes-3d 4 --inhibit-3d True --detection-width-3d 128 --stdtimes-filt-3d 0.3 --remove-edge-3d False
```
Further description for these arguments are shown in the picker.py file.

Another way to input such arguments is to put them in a file (e.g. sample.params) and mention such file when you run the command.
Inside the file:
```
--imod-location /Applications/IMOD
--input /tomo/TS_86.rec
--output /output
--rad 90
--scope-pixel 1.7
--binning 8
--contract-times-3d 1
--gaussian-3d False
--sigma-3d 15
--stdtimes-cont-3d 0.75
--min-size-3d 125
--dilation-3d 70
--radiustimes-3d 4
--inhibit-3d True
--detection-width-3d 128
--stdtimes-filt-3d 0.3
--remove-edge-3d False
```
Then run the command ```python3 picker.py @sample.params```

### Observing the results
All files produced would be inside the directory defined for argument '--output' (e.g. /output). The command ```imod -xyz -Y /output/TS_86_area.mrc``` could be used to show the whole tomogram with particles marked on it. The command ```imod -xyz -Y /tomo/TS_86.rec /output/TS_86.spk``` could be used to show the whole tomogram with particles marked on it. Here in both commands the first term 'imod' should be '(imod location)/bin/imod' (e.g. '/Applications/IMOD/bin/imod') if it is not added into the environment variables.