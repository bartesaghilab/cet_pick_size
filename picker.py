import argparse
import os
import mrc
import numpy as np
import scipy
from skimage.morphology import remove_small_objects,ball
from distutils.util import strtobool

class Parser(argparse.ArgumentParser):
    def convert_arg_line_to_args(self, arg_line):
        return arg_line.split(" ")

def parse():
    parser = Parser(fromfile_prefix_chars='@')
    parser.add_argument("--imod-location", type=str, default="/Applications/IMOD",
        help="the location of the imod directory")
    parser.add_argument("--input", type=str, default="/tomo/TS_86.rec",
        help="location of the input rec(mrc) file")
    parser.add_argument("--output", type=str, default="/output",
        help="directory where to put output files")
    parser.add_argument("--rad", type=float, default=90,
        help="Size to use for particle detection (A)")
    parser.add_argument("--scope-pixel", type=float, default=2.1,
        help="actual length (A) for each voxel")
    parser.add_argument("--binning", type=int, default=8,
        help="time of contraction of the tomogram for the whole picking process")
    parser.add_argument("--contract-times-3d", type=int, default=1,
        help="Reduce size of tomogram before creating contamination mask (this will speed up processing)")
    parser.add_argument("--gaussian-3d", type=lambda x: bool(strtobool(x)), default=False,
        help="Use a gaussian filter to preprocess the tomogram before creating the contamination mask")
    parser.add_argument("--sigma-3d", type=int, default=15,
        help="Sigma parameter for the gaussian filter")
    parser.add_argument("--stdtimes-cont-3d", type=float, default=1,
        help="Value n where (greyvalue of voxel)<mean-n*std is a condition for a voxel to be determined as a contaminated one")
    parser.add_argument("--min-size-3d", type=int, default=125,
        help="Minimum size threshold for remove of small connected possible contamination areas")
    parser.add_argument("--dilation-3d", type=float, default=100,
        help="Range for the marked contamination areas to dilate")
    parser.add_argument("--radiustimes-3d", type=float, default=4,
        help="the times of radius used for controlling how close the distance between candidate particles should be (it should be 4 based on assumption, but other values could also be tried for utility)")
    parser.add_argument("--inhibit-3d", type=lambda x: bool(strtobool(x)), default=True,
        help="Whether to use Inhibition Method for candidate particle picking (use this method to get more candidates)")
    parser.add_argument("--detection-width-3d", type=float, default=128,
        help="the thickness range for candidate particle detection (center plane +/- width are detected)")
    parser.add_argument("--stdtimes-filt-3d", type=float, default=1,
        help="Value n where (std of greyvalues of a single candidate particle)>mean+n*std is a condition for a candidate particle to be determined as a particle")
    parser.add_argument("--remove-edge-3d", type=lambda x: bool(strtobool(x)), default=False,
        help="Whether to add criterion which rejects any particle which doesn't have its foreground std greater than its background std")
    args = parser.parse_args()
    return args

def run_shell_command(com):
    os.system(com)
def get_imod_path():
    return args.imod_location

def extract(image,boxes,boxsize):
    particles = np.zeros((len(boxes), boxsize, boxsize, boxsize))
    i=0
    for box in boxes:
        x,y,z= box[2],box[1],box[0]
        particles[i, :, :, :]=image[int(x-boxsize/2):int(x+boxsize/2),int(y-boxsize/2):int(y+boxsize/2),int(z-boxsize/2):int(z+boxsize/2)]
        i=i+1
    return particles

class Picker():
    def __init__(self,input,output,radius=100,pixelsize=2.1,auto_binning = 12):
        self.radius=radius
        self.pixelsize=pixelsize
        self.name=input.split("/")[-1][:-4]
        self.output=output
        self.auto_binning = auto_binning        
        
        self.rec = mrc.read(input)
        self.tilesize = int(3 * radius / pixelsize/ auto_binning)
        if self.tilesize % 2 > 0:
            self.tilesize += 1
            
        radius1 = 0.001
        radius2 = 0.5 * self.pixelsize * self.auto_binning / self.radius
        sigma1 = 0
        sigma2 = 0.001
        
        com = "{5}/bin/mtffilter -3dfilter -radius1 {1} -hi {2} -l {3},{4} {6} {7}/{0}_bp.mrc".format(
            self.name, radius1, sigma1, radius2, sigma2,get_imod_path(),input,self.output
        )
        
        run_shell_command(com)
        
        self.lowres = mrc.read(self.output+"/"+self.name+"_bp.mrc")
            
    def getcont(self,contract_times=1,gaussian=True,sigma=15,stdtimes=3.5,min_size=125,dilation=100):
        G = self.rec.reshape(int(self.rec.shape[0]/contract_times),contract_times,int(self.rec.shape[1]/contract_times),contract_times,int(self.rec.shape[2]/contract_times),contract_times).mean(1).mean(2).mean(3)
        if gaussian:
            Gf = scipy.ndimage.gaussian_filter(G, sigma=sigma)
            G = G-Gf
        maskthres=G.mean()-stdtimes*G.std()
        mask = G < maskthres
        cmask = scipy.ndimage.morphology.binary_opening(mask, ball(1))
        clean = remove_small_objects(cmask, min_size=min_size)
        segmentation = scipy.ndimage.morphology.binary_closing(clean, ball(2))
        contamination_dilation = int(dilation/contract_times / self.pixelsize/ self.auto_binning)
        area = scipy.ndimage.morphology.binary_dilation(segmentation, ball(contamination_dilation))
        mrc.write(area, self.output+"/"+self.name+"_area.mrc")
            
        return area
        
    def detect(self,area,contract_times=1,radius_times=4,inhibit=False,detection_width=128):
        points=self.minima_extract(radius_times=radius_times,inhibit=inhibit)
        boxes = []
        for i in range(len(points)):
            x = points[i][0]
            y = points[i][1]
            z = points[i][2] 
            
            clean = not area[int(x/contract_times),int(y/contract_times),int(z/contract_times)]
            inside = (
                x- self.tilesize / 2 >= 0
                and x < self.rec.shape[0] - self.tilesize/2 + 1
                and y- self.tilesize / 2 >= 0
                and y < self.rec.shape[1] - self.tilesize/2 + 1
                and z- self.tilesize / 2 >= 0
                and z < self.rec.shape[2] - self.tilesize/2 + 1
                and y <= self.rec.shape[1]*0.5+detection_width
                and y >= self.rec.shape[1]*0.5-detection_width
            )
            if clean and inside:
                boxes.append([z,y,x])
        
        raw_particles = extract(self.lowres, boxes,self.tilesize)
        return boxes,raw_particles
    
    def prefilt(self,raw_particles,stdtimes=1):
        x, y= np.mgrid[0:self.tilesize, 0:self.tilesize] - self.tilesize / 2 + 0.5
        condition2d = np.sqrt(x*x+y*y) > self.radius / self.pixelsize / self.auto_binning
        raw_particles2d=raw_particles.sum(axis=2)
        
        particles_metrics = np.zeros([raw_particles.shape[0],2])
        for p in range(raw_particles.shape[0]):
            raw = raw_particles2d[p, :, :]
            background = np.extract(condition2d, raw)
            foreground = np.extract(np.logical_not(condition2d), raw)
            particles_metrics[p,0] =foreground.std()
            particles_metrics[p,1] =background.std()
        stdmean,stdstd=particles_metrics[:,0].mean(),particles_metrics[:,0].std()
        
        stdthreshold=stdmean+stdstd*stdtimes
        return particles_metrics,stdthreshold

    def filt(self,boxes,particles_metrics,stdthreshold,remove_edge):
        boxs = []
        rawboxs={}
        for p in range(particles_metrics.shape[0]):
            std=particles_metrics[p,0]
            if std >= stdthreshold and (std>particles_metrics[p,1] or (not remove_edge)):
                if std not in rawboxs:
                    rawboxs[std]=[]
                rawboxs[std].append([boxes[p][0],boxes[p][1],boxes[p][2]])
        rawboxs=[rawboxs[k] for k in sorted(rawboxs.keys())]
        for b in rawboxs:
            boxs+=b
            
        f = open(self.output+"/"+self.name+"_boxs.txt", 'w')
        f.writelines([' '.join(str(boxs[i])[1:-1].split(', '))+'\n' for i in range(len(boxs))])
        f.close()
        circle = int(self.radius / self.pixelsize / self.auto_binning)
        run_shell_command(get_imod_path()+'/bin/point2model '+self.output+'/'+self.name+'_boxs.txt '+self.output+'/'+self.name+'.mod -sphere %s' % circle)
        run_shell_command(get_imod_path()+'/bin/imodtrans -Y -T '+self.output+'/'+self.name+ '.mod '+self.output+'/' + self.name + ".spk")
        #run_shell_command(get_imod_path()+'/bin/point2model output/'+self.name+'_boxs.txt output/'+self.name+'.spk -sp 10')
        
    def minima_extract(self,radius_times=4,inhibit=False):
        locality = int(radius_times*self.radius / self.pixelsize / self.auto_binning)
        if not inhibit:
            minimas = (
                self.lowres == scipy.ndimage.minimum_filter(self.lowres, locality)
            ).nonzero()
            points=[]
            for i in range(minimas[0].shape[0]):
                points.append([minimas[0][i],minimas[1][i],minimas[2][i]])
            return points
        locality2 = int(radius_times*self.radius/2 / self.pixelsize / self.auto_binning)
        minimas = (
            self.lowres == scipy.ndimage.minimum_filter(self.lowres, locality2)
        ).nonzero()
        rawpoints={}
        for i in range(minimas[0].shape[0]):
            x,y,z=minimas[0][i],minimas[1][i],minimas[2][i]
            v=self.lowres[x,y,z]
            if v not in rawpoints:
                rawpoints[v]=[]
            rawpoints[v].append([[x,y,z],False])
        editmax=self.lowres.max()
        edited=self.lowres+(1-(self.lowres == scipy.ndimage.minimum_filter(self.lowres, locality2)))*(editmax-self.lowres)
        inhibited=1
        while inhibited>0:
            inhibited=0
            minf=scipy.ndimage.minimum_filter(edited, locality)
            
            editmax=edited.max()
            
            for k in rawpoints.keys():
                for i in range(len(rawpoints[k])):
                    p=rawpoints[k][i]
                    minimum=minf[p[0][0],p[0][1],p[0][2]]
                    if minimum==k:
                        p[1]=False
                    elif minimum in rawpoints:
                        p[1]=minimum
                        inhibited+=1
                    else:
                        p[1]=True
                        inhibited+=1
                        x,y,z=p[0][0]-int(locality/2),p[0][1]-int(locality/2),p[0][2]-int(locality/2)
                        a,b,c=x+locality,y+locality,z+locality
                        x=0 if x<0 else x
                        y=0 if y<0 else y
                        z=0 if z<0 else z
                        a=edited.shape[0] if a>edited.shape[0] else a
                        b=edited.shape[1] if b>edited.shape[1] else b
                        c=edited.shape[2] if c>edited.shape[2] else c
                        edited[x:a,y:b,z:c]+=(edited[x:a,y:b,z:c]==minimum)*(editmax-edited[x:a,y:b,z:c])
                        
            deleting={}
            for k in rawpoints.keys():
                for i in range(len(rawpoints[k])):
                    p=rawpoints[k][i]
                    if type(p[1])!=bool:
                        d=False
                        for j in rawpoints[p[1]]:
                            if type(j[1])==bool and j[1]==False:
                                d=True
                                break
                        if d:
                            if k not in deleting:
                                deleting[k]=[]
                            deleting[k].append(i)
                            
            
            for k in deleting.keys():
                for i in range(len(deleting[k])-1,-1,-1):
                    point=rawpoints[k][deleting[k][i]][0]
                    edited[point[0],point[1],point[2]]=editmax
                    rawpoints[k].pop(deleting[k][i])
                if len(rawpoints[k])==0:
                    del rawpoints[k]
        points=[]
        for k in rawpoints.keys():
            for p in rawpoints[k]:
                points.append(p[0])
        return points

def pick(input,output,radius=100,pixelsize=2.1,auto_binning = 12,contract_times=1,gaussian=False,sigma=15,stdtimes_cont=3.5,min_size=125,dilation=100,radius_times=4,inhibit=False,detection_width=128,stdtimes_filt=1,remove_edge=False):
    p=Picker(input,output,radius=radius,pixelsize=pixelsize,auto_binning = auto_binning)
    area=p.getcont(contract_times=contract_times,gaussian=gaussian,sigma=sigma,stdtimes=stdtimes_cont,min_size=min_size,dilation=dilation)
    
    boxes,raw_particles=p.detect(area,contract_times=contract_times,radius_times=radius_times,inhibit=False,detection_width=detection_width)
    particles_metrics,stdthreshold=p.prefilt(raw_particles,stdtimes=stdtimes_filt)
    if inhibit:
        boxes,raw_particles=p.detect(area,contract_times=contract_times,radius_times=radius_times,inhibit=True,detection_width=detection_width)
        particles_metrics,_=p.prefilt(raw_particles,stdtimes=stdtimes_filt)
    p.filt(boxes,particles_metrics,stdthreshold,remove_edge)

if __name__ == '__main__':
    args=parse()
    pick(args.input,args.output,radius=args.rad,pixelsize=args.scope_pixel,auto_binning = args.binning,contract_times=args.contract_times_3d,gaussian=args.gaussian_3d,
        sigma=args.sigma_3d,stdtimes_cont=args.stdtimes_cont_3d,min_size=args.min_size_3d,dilation=args.dilation_3d,radius_times=args.radiustimes_3d,
        inhibit=args.inhibit_3d,detection_width=args.detection_width_3d,stdtimes_filt=args.stdtimes_filt_3d,
        remove_edge=args.remove_edge_3d)
    
    